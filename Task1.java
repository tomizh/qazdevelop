import java.util.Scanner;

/*
ЗАДАНИЕ #1. Java Core:
- CLI-приложение: принимает произвольный текст как аргумент командной строки, делит строку на абзацы,
выводит список полученных абзацев, для каждого абзаца выводится количество слов в этом абзаце.
*/

public class Task1 {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);  // create a Scanner object
        System.out.println("Enter text");
        String text = myObj.nextLine();  // read user input

        String[] arrOfTxt = text.split("!");

        for (String a : arrOfTxt) {
            int index = 0;
            int counter = 1;
            while(index < a.length() - 1) {
                // checking with ascii values of characters -> to count words
                if((a.charAt(index) < 65 || (a.charAt(index) > 90 && a.charAt(index) < 97) || a.charAt(index) > 122) && ((a.charAt(index+1) > 64
                        && a.charAt(index+1) < 91 ) || a.charAt(index+1) > 96 && a.charAt(index+1) < 123)){
                    counter++;
                }
                index++;
            }
            System.out.println(a + "\nNumber of words in above paragraph: " + counter);
        }
    }
}
